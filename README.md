# sniff-price
Look at a site's prices for a specific item and category (might open to config
 later) and let me know when it drops down.

## Requirements
Currently we're using `notify-send` to pop up a notification. But, the script 
can be used as-is with whatever time wrapper and notification.

## Installation
`pipenv sync`

## Execution
```
while ;do pipenv run python sniff.py | while read hit_price; do echo "${hit_price}"; notify-send "${hit_price}"; done; echo "Sleeping 5 min before next iteration."; sleep 360; done
```
>I know. Quick and dirty
