#!/usr/bin/env python
"""
Looks at yad2 for 4k screen prices.
Saves them in a small DB.
Notifies when new and interesting deals pop up.
"""
import sqlite3
import requests
import datetime
from random import randint
from functools import lru_cache
from bs4 import BeautifulSoup

DB = "store.sqlite"
URL = ("https://www.yad2.co.il/products/electrical-appliances"
       "?category=6&item=36&info=4k")


def clean(ugly: str) -> str:
    """
    Cleans up a string by stripping it of extra \n and ' '
    as well as removing the NIS sign ₪.
    """
    ugly = ugly.replace('₪', '')
    pretty = ugly.strip()
    return pretty


def connect(db: str=DB):
    """
    Connects to the database which allows creating a cursor.
    """
    return sqlite3.connect(db)


def init_db(db: str=DB):
    """
    Create the table used in the app
    """
    create = """
        CREATE TABLE IF NOT EXISTS screens (
            name TEXT PRIMARY KEY,
            price INT,
            found_time TEXT,
            ignore INT DEFAULT 0
        )"""
    with connect(db) as sdb:
        cursor = sdb.cursor()
        cursor.execute(create)


def existing_screens(db: str=DB) -> dict:
    """
    Read previously found screens from the DB
    """
    data = {}
    sql = "SELECT name, price, found_time FROM screens WHERE ignore = 0"
    with connect(db) as sdb:
        cursor = sdb.cursor()
        for row in cursor.execute(sql):
            data[row[0]] = row[1]
        return data


def upsert_screens(data: dict, db: str):
    """
    Adds new screens to the DB.
    """
    upsert = """
        INSERT INTO screens
        (name, price, found_time)
        VALUES (?, ?, ?)
        ON CONFLICT(name) DO
        UPDATE SET price=?, found_time=?;"""
    now = datetime.datetime.now()
    with connect(db) as sdb:
        cursor = sdb.cursor()
        for name, price in data.items():
            payload = (name, price, now, price, now)
            cursor.execute(upsert, payload )


def fetch_prices(url: str) -> requests.models.Response:
    """
    GET the prices from the designated URL with a nice user-agent.
    """
    rand_ver = randint(1000, 9999)
    agent = (f"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 "
             "(KHTML, like Gecko) Chrome/70.0.{rand_ver}.77 Safari/537.36")
    # Thank you, https://stackoverflow.com/a/10606260
    headers = requests.utils.default_headers()
    # Update the headers with your custom ones
    # You don't have to worry about case-sensitivity with
    # the dictionary keys, because default_headers uses a custom
    # CaseInsensitiveDict implementation within requests' source code.
    headers.update({'User-Agent': agent})
    with requests.Session() as sess:
        return sess.get(url, headers=headers)


def crawl_prices(html_doc: str) -> dict:
    """
    Load a web page into BeautifulSoup and return {item_name: price}
    Expects the table rows to be of class 'feeditem table'.
    Expects the name of the item to be a child of said table row with
    class 'row-1'.
    Expects the price of the item to be a child of said table row with
    class 'price'.
    """
    data = {}
    soup = BeautifulSoup(html_doc, 'html.parser')
    items = soup.find_all('div', {'class': 'feeditem table'})
    for tag in items:
        name = tag.find('div', {'class': 'row-1'}).text
        price = tag.find('div', {'class': 'price'}).text
        name = clean(name)
        price = clean(price).replace(',', '')
        data[name] = int(price)

    return data


def star_design(db: str=DB, url: str=URL):
    """
    Fetch prices from yad2 of 4k screens and upsert them
    into a local DB.
    """
    r = fetch_prices(url)
    if r.status_code != 200:
        raise Exception(f"Request code!= 200. {r.reason}.")
    init_db(db)
    # Website snapshot at 19-11-14 22:47
    # with open('snap.html', 'r', encoding='utf-8') as fr:
    #     new_prices = crawl_prices(fr.read())
    new_prices = crawl_prices(r.text)
    upsert_screens(new_prices, db)

    for name, price in existing_screens(db).items():
        if price < 700:
            print(f"{price} {name}.", flush=True)


if __name__ == '__main__':
    star_design(DB, URL)
